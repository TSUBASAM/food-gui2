import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI2 {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextArea textArea1;

    //クラスを使ってまとめて表示する
    void order (String food){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                food + "食べたいの？？",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            textArea1.setText(food + "は得意料理だから任せな！");
            JOptionPane.showMessageDialog(null,"おっけー" + food + "作るね！");
        }
    }
    public FoodGUI2() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("天ぷら");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ラーメン");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("うどん");
            }
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI2");
        frame.setContentPane(new FoodGUI2().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
